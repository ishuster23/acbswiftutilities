//
//  EmailLoginProvider.swift
//  ACBSwiftUtilities
//
//  Created by Alejandro Cárdenas on 4/19/16.
//
//

import Foundation

public struct EmailLoginProvider: LoginProvider {
  public var user: LoginUser
  
  public func login() {
    print("login with email: \(user.email)")
  }
  
  public var isValid: Bool {
    get {
      return user.isValid()
    }
    
    set {
      self.isValid = newValue
    }
  }
}