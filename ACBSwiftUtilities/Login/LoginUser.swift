//
//  LoginUser.swift
//  ACBSwiftUtilities
//
//  Created by Alejandro Cárdenas on 4/19/16.
//
//

import Foundation

public struct LoginUser {
  public let email: String
  public let password: String
  public func isValid() -> Bool {
    return !email.isEmpty && !password.isEmpty
  }
}