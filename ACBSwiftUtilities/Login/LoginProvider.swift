//
//  LoginProvider.swift
//  ACBSwiftUtilities
//
//  Created by Alejandro Cárdenas on 4/19/16.
//
//

import Foundation

protocol LoginProvider {
  func login()
  var isValid: Bool { get set }
}