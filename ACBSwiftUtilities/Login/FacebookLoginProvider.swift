//
//  FacebookLoginProvider.swift
//  ACBSwiftUtilities
//
//  Created by Alejandro Cárdenas on 4/19/16.
//
//

import Foundation

public struct FacebookLoginProvider: LoginProvider {
  public func login() {
    print("log in with facebook")
  }
  
  public var isValid: Bool {
    get {
      return self.isValid
    }
    
    set {
      self.isValid = newValue
    }
  }
  
}